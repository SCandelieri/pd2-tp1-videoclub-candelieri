import express, { Request, Response, NextFunction } from 'express';
import cors from 'cors';
import xmlparser from 'express-xml-bodyparser';
import createError, { HttpError } from 'http-errors';
import Joi from 'joi';

import rentRouter from './rent/rent.route';

const app = express();

app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(xmlparser({ trim: true, explicitArray: false }));
app.use(cors());

app.use('/rent', rentRouter);

app.use((req: Request, res: Response, next: NextFunction) => next(createError(404, 'Not Found')));

app.use((error: Error, req: Request, res: Response, next: NextFunction) => {
	if(Joi.isError(error))
		next(createError(422, error.message));
	else
		next(error);
});

app.use((error: HttpError, req: Request, res: Response, next: NextFunction) => {
	const errorStatus: number = error.status || error.statusCode || 500;
	res.status(errorStatus).json({
		status: errorStatus,
		statusCode: errorStatus,
		message: error.message
	});
	next(error);
});

export default app;