import Joi from 'joi';

import IRent from './IRent';

const rentSchema = Joi.object({
	object_id: Joi.string().uuid().required(),
	client_id: Joi.string().uuid().required(),
	details: Joi.object({
		status: Joi.string().valid('RENT', 'DELIVERY_TO_RENT', 'RETURN', 'DELIVERY_TO_RETURN').required(),
		until: Joi.string().pattern(/[0-9]{4}\/[0-9]{2}\/[0-9]{2}/).required()
	}).required()
});

export default class RentController {
	public async insert(rent: IRent): Promise<IRent> {
		const validRent: IRent = await rentSchema.validateAsync(rent, { abortEarly: false });
		// Save to DB.
		return validRent;
	}
}