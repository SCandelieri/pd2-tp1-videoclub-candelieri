import { Router, Request, Response } from 'express';
import asyncHandler from 'express-async-handler';

import IRent from './IRent';
import RentController from './rent.controller';

const rentRouter = Router();
const rentController = new RentController();

rentRouter.post('/', asyncHandler(async (req: Request, res: Response) => {
	await rentController.insert(req.body.rent as IRent);
	res.sendStatus(201);
}));

export default rentRouter;