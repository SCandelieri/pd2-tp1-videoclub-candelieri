export default interface IRent {
	object_id?: string;
	client_id?: string;
	details?: {
		status?: string;
		until?: string;
	};
}