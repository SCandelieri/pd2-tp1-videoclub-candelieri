import { expect } from 'chai';
import Joi from 'joi';

import IRent from './IRent';
import RentController from './rent.controller';

describe('Rent', () => {
	const rentController = new RentController();

	describe('POST', () => {
		it('should create rent', async () => {
			const validRent: IRent = {
				client_id: '5d65ac9e-d431-4138-a8e4-c4719205cb1b',
				object_id: 'b172a2ab-5900-4532-bd68-68a041752017',
				details: {
					status: 'RENT',
					until: '2020/10/01'
				}
			};

			const rentResponse: IRent = await rentController.insert(validRent);

			expect(rentResponse).to.deep.equal(validRent);
		});

		it('should throw ValidationError required', async () => {
			try {
				const incompleteRent: IRent = {
					client_id: '5d65ac9e-d431-4138-a8e4-c4719205cb1b',
					details: {
						until: '2020/10/01'
					}
				};

				await rentController.insert(incompleteRent);
			}
			catch(error) {
				if(!Joi.isError(error))
					throw error;
				const validationError: Joi.ValidationError = error;
				expect(validationError.details.map(detail => detail.type)).to.contain('any.required');
			}
		});

		it('should throw ValidationError invalid uuid', async () => {
			try {
				const invalidUuidRent: IRent = {
					client_id: '5d65ac9e--a8e4-c4719205cb1b',
					object_id: 'b172a2ab-5900-4532-bd68-68a041752017',
					details: {
						status: 'RENT',
						until: '2020/10/01'
					}
				};

				await rentController.insert(invalidUuidRent);
			}
			catch(error) {
				if(!Joi.isError(error))
					throw error;
				const validationError: Joi.ValidationError = error;
				expect(validationError.details.map(detail => detail.type)).to.contain('string.guid');
			}
		});

		it('should throw ValidationError invalid date', async () => {
			try {
				const invalidDateRent: IRent = {
					client_id: '5d65ac9e-d431-4138-a8e4-c4719205cb1b',
					object_id: 'b172a2ab-5900-4532-bd68-68a041752017',
					details: {
						status: 'RENT',
						until: '2020-10-01'
					}
				};

				await rentController.insert(invalidDateRent);
			}
			catch(error) {
				if(!Joi.isError(error))
					throw error;
				const validationError: Joi.ValidationError = error;
				expect(validationError.details.map(detail => detail.type)).to.contain('string.pattern.base');
			}
		});

		it('should throw ValidationError invalid status', async () => {
			try {
				const invalidStatusRent: IRent = {
					client_id: '5d65ac9e-d431-4138-a8e4-c4719205cb1b',
					object_id: 'b172a2ab-5900-4532-bd68-68a041752017',
					details: {
						status: 'RENTED',
						until: '2020/10/01'
					}
				};
				await rentController.insert(invalidStatusRent);
			}
			catch(error) {
				if(!Joi.isError(error))
					throw error;
				const validationError: Joi.ValidationError = error;
				expect(validationError.details.map(detail => detail.type)).to.contain('any.only');
			}
		});
	});
});