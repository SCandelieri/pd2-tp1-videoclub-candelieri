import 'dotenv/config';

import app from './components/app';

const port: number = parseInt(process.env.PORT || '4242');

app.listen(port, () => console.log(`Server running on port ${port}`));