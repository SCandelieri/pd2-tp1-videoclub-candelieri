const nodeExternals = require('webpack-node-externals');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const path = require('path');

module.exports = {
	mode: 'production',
	entry: path.resolve(__dirname, 'src/index.ts'),
	target: 'node',
	stats: 'errors-warnings',
	resolve: {
		extensions: ['.ts', '.js', '.json']
	},
	output: {
		path: path.resolve(__dirname, 'dist'),
		publicPath: '/',
		filename: 'server.js'
	},
	externals: [nodeExternals()],
	module: {
		rules: [
			{
				test: /\.(js|ts)$/,
				exclude: [
					path.resolve(__dirname, 'node_modules'),
					path.resolve(__dirname, 'dist')
				],
				use: ['babel-loader', 'eslint-loader']
			}
		]
	},
	plugins: [
		new ForkTsCheckerWebpackPlugin({
			typescript: {
				diagnosticOptions: {
					semantic: true,
					syntactic: true
				}
			},
			logger: { infrastructure: 'silent' },
			async: false
		})
	]
};