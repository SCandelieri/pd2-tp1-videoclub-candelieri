# UNDAV - Programación Distribuida II - TP1 - API Videoclub

API REST para el alquiler de VHS en un videoclub.

Desarrollada utilizando las siguientes tecnologías:

- **Typescript** como lenguaje de desarrollo.
- **Node.js** como entorno de ejecución.
- **Express** como web framework para facilitar la construcción de la API.

La aplicación cuenta con los siguientes recursos:

- **Rent:** Accesible desde la ruta `/rent`.
  - `POST /rent` para crear un nuevo alquiler.

## Instalación

### Pre-requisitos

- **node.js** - version 12.x LTS - [Guia de instalacion](https://nodejs.org/en/download).
- **npm** - incluido con node.js.
- **Postman** - Para pruebas (opcional) - [Guia de instalacion](https://www.postman.com/downloads/).

### Preparación del ambiente

- Clonar el repositorio.

```bash
git clone https://gitlab.com/SCandelieri/pd2-tp1-videoclub-candelieri.git
```

- Moverse a la carpeta del proyecto.

```bash
cd pd2-tp1-videoclub-candelieri
```

- Instalar las dependencias.

```bash
npm install
```

- Iniciar el servidor de desarrollo.

```bash
npm start
```

## Pruebas

1. Abrir Postman.
2. Hacer click en _Import_.
3. Seleccionar la opcion _Desde carpeta_.
4. Seleccionar el archivo [pd2-tp1-videoclub-candelieri.postman_collection.json](./pd2-tp1-videoclub-candelieri.postman_collection.json) disponible en la raiz de este repositorio.
5. Una vez importado el proyecto, en la coleccion _Rent_ abrir la consulta _Crear un Alquiler_.
6. Asegurarse que el servidor de desarrollo se encuentre en ejecucion.
7. Hacer click en _Enviar_, deberia devolver el mismo XML como respuesta.
